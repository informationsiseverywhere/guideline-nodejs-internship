version: 22.04
updated on: 2022-04-18

- [ ] is optional
- [x] is required

# WM VA-Delivery Training Action Checklist

- [x] Welcome (1/2 1st week)
  - [x] DXC & Our Team introduction
  - [x] Self Introduction (Personality, Hobbies, still studying at school or not, his available time...)
  - [x] Collect trainee phone number, email...
  - [x] Product & Delivery overview
- [x] Training syllabus (rest)
  - [x] DXC Accounts, Etes, DXC University, Sapa Cloud, GoFluences...
  - [x] DXC Devices
  - [x] Rules, Policies, Do's Dont's, organize Email + Teams status, Workday, DXC Resources + contact point
  - [x] Set goal, career path
  - [x] Development Processes & Project Documents
  - [x] Development Environment & Setup
  - [x] Required Courses
  - [x] Project Introduction & Practice plan -> Knowledge & Skill Self-Evaluation
  - [x] SoftSkill & others...
  - [x] Report/evaluate daily/weekly
- [x] Give/Get general feedback
  - [x] Personality/Attitude/Self study Ability
  - [x] Knowledge/Technology/Mindset
  - [x] Soft-skill/English/Communication
  - [x] Suggestion/Get feedback from mentee/Discuss

## Technical Traning Syllabus

Please refer [here](TechnicalTraning/README.md)

## Softskills Training Syllabus
