version: 22.04
updated on: 2022-04-21

## Abbreviation

# SoftSkills Training

## Contents

- [x] Agile/Scrum
- [ ] Time Management
- [x] Languages
  - [x] English
    - [ ] Reading
    - [ ] Listening
    - [ ] Speaking
    - [ ] Writing
  - [ ] Japanese