version: 22.04
updated on: 2022-04-21

# IBM Cloud & Watson Assistant

> IBM Cloud® offers the most open and secure public cloud for business, a next-generation hybrid multicloud platform, advanced data and AI capabilities, and deep enterprise expertise across 20 industries.

![IBM cloud](https://upload.wikimedia.org/wikipedia/commons/2/24/IBM_Cloud_logo.png)

A full stack cloud platform with over 170 products and services covering data, containers, AI, IoT, and blockchain.

Use your existing infrastructure—even edge or other public clouds—with IBM Cloud services, APIs, access policies, security controls and compliance with IBM Cloud Satellite.

IBM Cloud gives you more ways to elevate the value of your data with new insights and AI.

- 5 Cloud Options: Public cloud. Private cloud. Public dedicated. Hybrid. Multicloud. Match the right workload to the right cloud environment for your business.
- 20 industries benefit from cloud tech solutions with deep expertise deployed by IBM Cloud.
- 47 of the Fortune 50 companies trust IBM Cloud to be their enterprise-grade cloud system.
- 60 data centers worldwide to deploy locally and scale globally for resiliency and redundancy on 6 continents.
- 8000 security experts make IBM not only a cloud provider, but also a leading security company.

## References

- [IBM Cloud](https://www.ibm.com/cloud)

## Watson Assistant

> Give customers the answers they need quickly, easily, and across any channel

![Watson Assistant](https://www.ibm.com/watson/sg-en/ai-assistant/img/icon2.png)

Using artificial intelligence and natural language processing (NLP), IBM Watson® Assistant provides customers with the best customer experience. Remove the frustration of long wait times, tedious searches, and unhelpful chatbots. By learning from customer conversations, Watson Assistant automatically improves its ability to resolve issues the first time.

Build a better customer experience without a degree in computer science. Get up and running in minutes by using a drag-and-drop interface to build dialog. Then customize your chatbot to fit your brand and embed it in your website with a simple copy and paste.

Watson Assistant converses as naturally as your best support agent. With best-in-class NLU, Watson Assistant understands colloquial speech patterns and surfaces dynamic rich media — video, image, audio — to automate the customer experience while engaging your customers.

Integrate with anything and answer more than FAQs. Watson Assistant searches across your organization for information to make each conversation better — and provides detailed analytics to give you a holistic view.

Protect your data with IBM security, scalability, and flexibility. Support tens of millions of monthly conversations across your organization with a single product while safeguarding against customer data misuse.

## References

- [Watson Assistant](https://www.ibm.com/cloud/watson-assistant)
- [Watson Assistant Document](https://cloud.ibm.com/docs/assistant)

# ServiceNow

![snow-0](https://unifysolutions.net/supportedproduct/servicenow/servicenow-1.svg)

ServiceNow is a platform-as-a-service provider, providing technical management support, such as IT service management, to the IT operations of large corporations, including providing help desk functionality. The company's core business revolves around management of "incident, problem, and change" IT operational events.

## References

- [ServiceNow](https://www.servicenow.com/)
- [ServiceNow Document](https://docs.servicenow.com)
- [ServiceNow Query Document](https://docs.servicenow.com/en-US/bundle/sandiego-platform-administration/page/administer/exporting-data/concept/query-parameters-display-value.html)