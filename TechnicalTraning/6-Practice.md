version: 22.04
updated on: 2022-04-27

# Prerequisites

- [x] VSCode
      ![vscode](https://code.visualstudio.com/assets/home/home-screenshot-mac-lg-2x.png)

- [x] NodeJS LTS (12.x or above)
      ![nodejs](https://upload.wikimedia.org/wikipedia/commons/thumb/d/d9/Node.js_logo.svg/2880px-Node.js_logo.svg.png)

- [x] Function Core Tool v2
      ![azfunction](https://www.twoconnect.com/wp-content/uploads/2022/02/Serverless-Functions.png)

# Practice

## Local Development Environemnt

### Visual Studio Code Workspace & Debugging Compound script

Example:

```
{
  "folders": [
    { "path": "VA-2101-Conversation", "name": "Conversation" },
    { "path": "VA-2101-ITSM/azure/webapp", "name": "ITSM" },
    { "path": "VA-2101-ITSM-SNOW/azure/webapp", "name": "ITSM-ServiceNow" },
    { "path": "VA-2101-VA-Core/azure/function", "name": "Core" }
  ],
  "launch": {
    "version": "0.2.0",
    "compounds": [
      { "name": "Compound VA", "configurations": ["ITSM", "ITSM-ServiceNow", "Core"] },
      { "name": "Compound ITSM & ITSM-ServiceNow", "configurations": ["ITSM", "ITSM-ServiceNow"] }
    ]
  }
}

```

### VA Core Cache

Note that using one of these, you have to comment out the TSL block code inside the cache handler and make sure that the PORT and HOST_NAME are correct.

#### (Cloud) Using free Redislab

Register a free redis database [here](https://redis.com).

#### (Locally) Locally Installation

#### (Locally) Using Docker container

Execute below command:

```
docker run -d \
  -h redis \
  -e REDIS_PASSWORD=redis \
  -v redis-data:/data \
  -p 6379:6379 \
  --name redis \
  --restart always \
  redis:alpine /bin/sh -c 'redis-server --appendonly yes --requirepass ${REDIS_PASSWORD}'
```

## Regex/Regex Pattern

According to MDN Documents, REGEX - Regular expressions are patterns used to match character combinations in strings. In JavaScript, regular expressions are also objects. These patterns are used with the `exec()` and `test()` methods of RegExp, and with the `match()`, `matchAll()`, `replace()`, `replaceAll()`, `search()`, and `split()` methods of String. This chapter describes JavaScript regular expressions.

## References

- [Azure Functions Extension](https://marketplace.visualstudio.com/items?itemName=ms-azuretools.vscode-azurefunctions)
- [Regular Expression](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_Expressions)
- [Regexr.com](https://regexr.com)

# Build a Bot

## High level design

![hld](./assets/HLD.png)

## Technologies

![tech](./assets/d-001.png)

## Workflow

![workflow](./assets/d-002.png)

## User stories

Topic: Pokemon Bot

- As a user I want to say hello to the bot.
- As a user I want to ask the bot for general information of a pokemon (eg: name and short description).
- As a user I want to ask the bot for detail information of a pokemon (eg: heigh, weight, location).
- As a user I want to say good bye to the bot.
- As a user I want to get the history of the current conversation.

Extend:
- Adding multi-language support to the bot.

# DXC WM Virtual Agent

Example

![va](./assets/d-003.png)