version: 22.04
updated on: 2022-04-21

# RESTful API

> A REST API (also known as RESTful API) is an application programming interface (API or web API) that conforms to the constraints of REST architectural style and allows for interaction with RESTful web services. REST stands for representational state transfer and was created by computer scientist Roy Fielding.
>
> | REDHAT

![imaeg0](https://www.mindinventory.com/blog/wp-content/uploads/2021/09/rest-api-model-1.png)

Like other architectural styles, REST has its guiding principles and constraints. These principles must be satisfied if a service interface needs to be referred to as RESTful.

The six guiding principles or constraints of the RESTful architecture are:
1. Uniform Interface
2. Client-server
3. Stateless
4. Cacheable
5. Layered system
6. Code on demand (optional)

## What is a Resource?

The key abstraction of information in REST is a resource. Any information that we can name can be a resource. For example, a REST resource can be a document or image, a temporal service, a collection of other resources, or a non-virtual object (e.g., a person).

The state of the resource, at any particular time, is known as the resource representation.

The resource representations are consist of:

- the data
- the metadata describing the data
- and the hypermedia links that can help the clients in transition to the next desired state.

A REST API consists of an assembly of interlinked resources. This set of resources is known as the REST API’s resource model.

### Resource Identifiers

REST uses resource identifiers to identify each resource involved in the interactions between the client and the server components.

### Hypermedia

The data format of a representation is known as a media type. The media type identifies a specification that defines how a representation is to be processed.

A RESTful API looks like hypertext. Every addressable unit of information carries an address, either explicitly (e.g., link and id attributes) or implicitly (e.g., derived from the media type definition and representation structure).

### Self-descriptive

Further, resource representations shall be self-descriptive: the client does not need to know if a resource is an employee or a device. It should act based on the media type associated with the resource.

So in practice, we will create lots of custom media types – usually one media type associated with one resource.

Every media type defines a default processing model. For example, HTML defines a rendering process for hypertext and the browser behavior around each element.

### Resource Methods

- HTTP GET
- HTTP POST
- HTTP PUT
- HTTP PATCH
- HTTP DELETE

## Best practices

- Build an application to support the following:
    - GET
    - POST
    - PUT
    - PATCH
    - DELETE
- Example: getting user/users by email from azure active directory.

## References:
- [[DXC University] - API Development: REST API Semantics](https://share.percipio.com/cd/Fe5ZO5pPj)
- [[DXC University] - API Design: API Development](https://share.percipio.com/cd/TU4nOvd1p)
- [[Microsoft] RESTful web API design](https://docs.microsoft.com/en-us/azure/architecture/best-practices/api-design)