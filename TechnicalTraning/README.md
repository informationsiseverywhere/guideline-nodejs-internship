version: 22.04
updated on 2022-04-21

## Abbreviation

- `os`: operating system
- `ide`: integrated development environment
- `va`: virtual agent
- `ms/msvc`: microservice
- `wsl`: windows subsystem for linux
- `cli`: command line interface
- `api`: application programming interface
- `nvm`: node version management
- `az`: azure

# NodeJS - VA Delivery Training content:

- [x] is required
- [ ] is optional

### Content:

- [x] 1 - Getting started
  - [x] InternshipProgram/Team/Project introduction
  - [x] Setup
    - [x] DXC accounts
    - [x] Resources: Percipio, DXC University, GoFLUENCE
    - [x] Email signature
  - [x] Development Environment preparation
    - [x] VSCode
    - [ ] WSL
    - [ ] Docker
    - [ ] Redis cache
    - [x] Practice & Self-Study
- [x] 2 - NodeJS & RESTfulAPI 101
  - [x] What is NodeJS
  - [x] GET
  - [x] POST
  - [ ] PUT
  - [ ] PATCH
  - [ ] DELETE
  - [x] Postman
  - [x] Practice & Self-Study
- [x] 3 - Microservices, integration and tools
  - [x] Git & Source Control
    - [x] Practice & Self-Study
  - [ ] Jira
  - [ ] Confluence
  - [x] Microservice
  - [x] Gateway
- [x] 4 - IBM Cloud
  - [x] Watson Assistant
  - [ ] ServiceNow Integration
  - [x] Practice & Self-Study
- [x] 5 - Azure Cloud & Office 365
  - [ ] Az Fundamentals
  - [x] Az Portal
  - [x] Az Function
  - [ ] Az Cosmos DB
  - [ ] Az API Management (Gateway)
  - [ ] Az DevOps
  - [ ] Office 365 Integration
  - [x] Practice & Self-Study
- [x] 5 - Practicing
