version: 22.04
updated on: 2022-04-21

# Microservices

> Microservices - also known as the microservice architecture - is an architectural style that structures an application as a collection of services that are

![Microservices](https://microservices.io/i/Microservice_Architecture.png)

- Highly maintainable and testable
- Loosely coupled
- Independently deployable
- Organized around business capabilities
- Owned by a small team

The microservice architecture enables the rapid, frequent and reliable delivery of large, complex applications. It also enables an organization to evolve its technology stack.

![MS Microservices](https://docs.microsoft.com/en-us/azure/architecture/includes/images/microservices-logical.png)

## API Gateway

An API gateway is an API management tool that sits between a client and a collection of backend services. An API gateway acts as a reverse proxy to accept all application programming interface (API) calls, aggregate the various services required to fulfill them, and return the appropriate result.

## Refereces:

- [microservices.io](https://microservices.io)
- [[Microsoft] Microservices style](https://docs.microsoft.com/en-us/azure/architecture/guide/architecture-styles/microservices)
- [[Microsoft] .NET microservices](https://docs.microsoft.com/en-us/dotnet/architecture/microservices/)
- [What does an API gateway do?](https://www.redhat.com/en/topics/api/what-does-an-api-gateway-do)

# Docker

Docker is a set of platform as a service products that use OS-level virtualization to deliver software in packages called containers. The service has both free and premium tiers. The software that hosts the containers is called Docker Engine.

## Refereces:

- [Docker](https://www.docker.com/)
- [Documents](https://docs.docker.com)
- [Hub](https://hub.docker.com)

# Git and Source Control

![sourcecontrol](https://miro.medium.com/max/800/1*WH75kMtJErLpjflIWwduSg.png)

Git is software for tracking changes in any set of files, usually used for coordinating work among programmers collaboratively developing source code during software development. Its goals include speed, data integrity, and support for distributed, non-linear workflows.

## References

- [Git](https://git-scm.com)
- [Top 20 git commands](https://medium.com/edureka/git-commands-with-example-7c5a555d14c)

# Jira

> Jira is a workflow management system that lets you track your work in any scenario.

![Jira UI](https://wac-cdn-2.atlassian.com/image/upload/f_auto,q_auto/dam/jcr:858144d1-e857-4ab8-8861-2a71112e7a37/JSW-tour-board.png?cdnVersion=38)

Jira allows you to break your work down into manageable chunks, assign it to the right person, and progress it through a customizable workflow until it's done. Add statuses, priorities, comments, and attachments to make sure you have all the correct information to get your work done!

## References

- [Jira document](https://confluence.atlassian.com/jira)

# Confluence

> Confluence is where you create, organize, and discuss work with your team.

![Confluence UI](https://i0.wp.com/atlassianblog.wpengine.com/wp-content/uploads/05-AppSwitcher-1.png?w=1280&ssl=1)

## References

- [Confluence document](https://confluence.atlassian.com/doc/confluence-data-center-and-server-documentation-135922.html)
