version: 22.04
updated on: 2022-04-21

# Azure Cloud

## Azure Fundamentals

![az-fundamental-0](https://docs.microsoft.com/en-us/media/learn/certification/badges/microsoft-certified-fundamentals-badge.svg)

Whether you’re new to the field or a seasoned professional, mastering the basics in Microsoft Azure can help you jump-start your career and prepare you to dive deeper into the many technical opportunities Azure offers.
The certification validates your basic knowledge of cloud services and how those services are provided with Azure. Candidates should be able to demonstrate a fundamental knowledge of cloud concepts, along with Azure services, workloads, security, privacy, pricing, and support.

#### The Microsoft Certified: Azure Fundamentals certification could be a great fit for you if you’d like to:

- Prove your knowledge of cloud computing concepts, models, and services, such as public, private, and hybrid cloud, in addition to infrastructure as a service (IaaS), platform as a service (PaaS), and software as a service (SaaS).
- Show your expertise on how Azure supports security, privacy, compliance, and trust.

It is recommended to have familiarity with concepts of networking, storage, compute, application support, and application development. You can use your Azure Fundamentals certification to reinforce your basics for other Azure role-based or specialty certifications, but it isn’t a prerequisite for any of them.

#### To ensure you are prepared for the exam, we recommend:

- Fully understanding the skills measured.
- Studying the relevant self-paced content on Microsoft Learn, or attending a Microsoft Azure Virtual Training Day: Fundamentals, or signing up for an instructor-led training event with a Microsoft Learning Partner.
- Taking the practice exam to validate your knowledge and understanding of the exam experience.
- Get a trial subscription and give it a try.
- Checking out Master the basics with the Azure Fundamentals certification to learn more about this certification and how to get ready.

## Azure Functions

![azure-function](https://ms-azuretools.gallerycdn.vsassets.io/extensions/ms-azuretools/vscode-azurefunctions/1.5.2/1634164816134/Microsoft.VisualStudio.Services.Icons.Default)

![azure-function-0](https://azurecomcdn.azureedge.net/cvt-037bd3f26673cdb2f7c365a6dd123279f04cdfe700006fa74c7e0bb71037b868/images/page/services/functions/value-prop-2.svg)

Azure Functions is a serverless solution that allows you to write less code, maintain less infrastructure, and save on costs. Instead of worrying about deploying and maintaining servers, the cloud infrastructure provides all the up-to-date resources needed to keep your applications running.

![azure-function-1](https://azurecomcdn.azureedge.net/cvt-037bd3f26673cdb2f7c365a6dd123279f04cdfe700006fa74c7e0bb71037b868/images/page/services/functions/value-prop-5.svg)

### Azure Functions Core Tool

- [Work with Azure Function Core Tool](https://docs.microsoft.com/en-us/azure/azure-functions/functions-run-local?tabs=v3%2Cmacos%2Ccsharp%2Cportal%2Cbash%2Ckeda)
- [Azure Functions Core Tool](https://github.com/Azure/azure-functions-core-tools)

### References

- [Azure Fundamentals Resources](https://docs.microsoft.com/en-us/learn/certifications/azure-fundamentals/)
- [Schedule Exam](https://docs.microsoft.com/en-us/learn/certifications/exams/az-900)
- [Azure Functions](https://docs.microsoft.com/en-us/azure/azure-functions/)
- [Quickstart: Create a JavaScript function in Azure using Visual Studio Cod](https://docs.microsoft.com/en-us/azure/azure-functions/create-first-function-vs-code-node)

# Office 365

![office365-0](https://cdn.graph.office.net/prod/media/microsoft-365/hero/M365_MHero4_640x449.webp)
The Microsoft 365 Developer Program includes a Microsoft 365 E5 developer subscription that you can use to create your own sandbox and develop solutions independent of your production environment. You can build Microsoft Teams apps, Office Add-ins for Word, Excel, PowerPoint, or Outlook, or SharePoint Add-ins, using Microsoft Graph, the SharePoint Framework, Power Apps, and more.
Do you have a Visual Studio Pro or Enteprise subscription? If so, you can take advantage of additional benefits when you join the program; for details, see Join with Visual Studio.

### References

- [Office 365 DevCenter](https://developer.microsoft.com/en-us/microsoft-365)
- [Office 365 Developer Program](https://docs.microsoft.com/en-us/office/developer-program/microsoft-365-developer-program)
