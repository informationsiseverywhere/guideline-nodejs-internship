version: 22.04
updated on: 2022-04-21

# Development Environment preparation

## Prerequisite and note:

- [x] OS: `Windows 10 (at least 20H1 build number 19041, if using Docker, we need WSL2)`, `Linux` or `MacOS`
- [x] IDE: `Visual Studio code`
- [x] Shell: `PowerShell` or `Windows Terminal 1.0`, terminal 1.0 can be found on Microsoft App Store.

**(\*)** is optional

## Installation:

### VSCode

Follow instruction [here](https://code.visualstudio.com) to download and install.

### (\*) Docker

VA Need a redis instance, we can use docker to deploy an instance locally without install or register a cluster on cloud services. Follow below instruction to turn on WSL2 and setup Docker.

1. Turn on WSL 2 (this feature is available on windows 10 20H1) by run this command: `wsl.exe --install` under Administrator in Powershell.
2. Once done, reboot and log in again.
3. Open Microsoft App Store, install a distro of Linux, eg: Ubuntu.
4. Verify the WSL and Linux distro are running version 2 by run `wsl -l -v`.
5. Download and install Docker Desktop [here](https://hub.docker.com/editions/community/docker-ce-desktop-windows/)
6. Following steps 1 -> 8 in the instruction [here](https://docs.docker.com/desktop/windows/wsl/#install) to install and setup Docker desktop.

References:

- [Docker Desktop WSL2 Backend](https://docs.docker.com/desktop/windows/wsl/)
- [Docker Desktop Dashboard](https://docs.docker.com/desktop/dashboard/)
- [References, CLI & API](https://docs.docker.com/reference/)
- [Install WSL](https://docs.microsoft.com/en-us/windows/wsl/install)

### NodeJS

Note that Azure Functions required NodeJS 12.x.

#### Directly install

Follow instruction [here](https://nodejs.org/en/download/) to download and install.

#### Using WSL

If using WSL for development environment, we don't need to install NodeJS on Windows.

Follow these step after setup WSL2 (refer step 1-4 in Docker section above) and Docker Desktop:

1. Verify Linux distro (eg: Ubuntu) is running.
2. Run command `wsl` in terminal or shell.
3. After login to WSL (eg: Ubuntu). Run command:

```
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.0/install.sh | bash
```

4. Run command or just close the terminal/cmd window and open again:

```
export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm
```

5. Verify nvm is installed correctly by run command: `command -v nvm`.
6. Install NodeJS version 12 (12.22.7) by running command: `nvm install 12.22.7`.
7. Set default NodeJS in nvm: `nvm alias default 12.22.7`
8. Verify NodeJS 12 is activated: `node -v`

References:

- [nvm](https://github.com/nvm-sh/nvm)
- [node](https://nodejs.org/en/)

### Azure Function Core Tools

Refer Section 5.
